package dao;

import java.util.ArrayList;
import java.util.List;

import model.Member;

public class MemberDAO {
	// 登録したメンバーすべてを呼び出す
	public List<Member> findAll() {
		// 複数メンバーを登録するリスト
		List<Member> memberList = new ArrayList<>();
		// リストにインスタンスを登録
		memberList.add(new Member(1, "湊 雄輔", 23, "1.jpg", "入社2年目の23歳。RPGを作りたかった。"));
		memberList.add(new Member(2, "綾部めぐみ", 22, "2.jpg", "入社1年目の22歳。猛虎弁を使いこなす。"));
		memberList.add(new Member(3, "菅原拓真", 32, "3.jpg", "経験豊富なエンジニア。好きな言葉は「でも例外もあるんだ。」"));
		memberList.add(new Member(4, "朝香あゆみ", 25, "4.jpg", "湊の同僚だったが現在行方不明。"));

		return memberList;
	}

	// 登録メンバーを1名呼び出す
	public Member find(int index) {
		Member member = findAll().get(index - 1);
		return member;
	}
}
