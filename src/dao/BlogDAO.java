package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Blog;

public class BlogDAO {
	// データベース情報
    private final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
    private final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
    private final String DB_NAME = "sukkiri";//データベース名
    private final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
    private final String DB_USER = "root";//ユーザーID
    private final String DB_PASS = "root";//パスワード

    // ブログ一覧を取得
    public List<Blog> findAll() {
    	Connection conn = null;
    	List<Blog> blogList = new ArrayList<>();

    	try {
    		Class.forName(DRIVER_NAME);
    		conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);

    		//　SELECT文の準備
    		String sql = "SELECT id, create_at, title, article FROM blog ORDER BY create_at DESC";
    		PreparedStatement pStmt = conn.prepareStatement(sql);

    		// SELECT文の実行
    		ResultSet rs = pStmt.executeQuery();

    		// SELECT文の結果をArrayListに格納
    		while(rs.next()) {
    			int id = rs.getInt("id");
    			String createAt = rs.getString("create_at");

    			// 記事タイトル、記事内容
    			String title = rs.getString("title");
    			String article = rs.getString("article");

    			// Blogインスタンスに格納
    			Blog blog = new Blog(createAt, title, article);

    			// DBのidをBlogインスタンスに格納
    			blog.setId(id);
    			blogList.add(blog);
    		}
    	} catch(ClassNotFoundException | SQLException e) {
    		e.printStackTrace();
    		return null;
    	} finally {
    		// 接続処理後データベース切断
    		if(conn != null) {
    			try {
    				conn.close();
    			} catch(SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}

    	return blogList;
    }

    // idを検索してブログ記事を1件取得
    public Blog find(int index) {
    	Connection conn = null;
    	Blog blog = null;

    	try {
    		Class.forName(DRIVER_NAME);
    		conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);

    		// 条件付きSELECT文の準備
    		String sql = "SELECT id, create_at, title, article FROM blog WHERE id=?";
    		PreparedStatement pStmt = conn.prepareStatement(sql);

    		// ?に入るidを引数indexで指定
    		pStmt.setInt(1, index);

    		// SELECT文の実行
    		ResultSet rs = pStmt.executeQuery();

    		// SELECT文の結果をArrayListに格納
    		while(rs.next()) {
    			int id = rs.getInt("id");
    			String createAt = rs.getString("create_at");
    			String title = rs.getString("title");
    			String article = rs.getString("article");

    			blog = new Blog(createAt, title, article);

    			// DBのidをBlogインスタンスに格納
    			blog.setId(id);
    		}
    	} catch(ClassNotFoundException | SQLException e) {
    		e.printStackTrace();
    		return null;
    	} finally {
    		// 接続処理後データベース切断
    		if(conn != null) {
    			try {
    				conn.close();
    			} catch(SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}

    	return blog;
    }

    // ブログ記事を作成
    public boolean create(Blog blog) {
    	Connection conn = null;

    	try {
    		// データベース接続
    		Class.forName(DRIVER_NAME);
    		conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);

    		// INsert文の準備(idは自動連番なので指定しなくてよい)
    		String sql = "INSERT INTO blog(create_at, title, article) VALUES(?, ?, ?)";
    		PreparedStatement pStmt = conn.prepareStatement(sql);

    		// INSERT文中の「?」に使用する値を設定しSQLを完成
    		pStmt.setString(1, blog.getCreateAt());
    		pStmt.setString(2, blog.getTitle());
    		pStmt.setString(3, blog.getArticle());

    		// INSERT文を実行
    		int result = pStmt.executeUpdate();

    		if(result != 1) {
    			return false;
    		}
    	} catch(SQLException | ClassNotFoundException e) {
    		e.printStackTrace();
    		return false;
    	} finally {
    		if(conn != null) {
    			try {
    				conn.close();
    			} catch(SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}

    	return true;
    }
}
