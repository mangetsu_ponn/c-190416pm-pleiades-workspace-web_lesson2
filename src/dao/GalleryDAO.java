package dao;

import java.util.ArrayList;
import java.util.List;

import model.Gallery;

public class GalleryDAO {
	// 登録した画像すべてを呼び出す
	public List<Gallery> findAll() {
		// 複数画像を登録するリスト
		List<Gallery> galleryList = new ArrayList<> ();
		
		// リストにインスタンスを登録
		galleryList.add(new Gallery(1, "画像1", "1.jpg", "なんたらかんたらなんたらかんたらなんたらかんたら"));
		galleryList.add(new Gallery(2, "画像2", "2.jpg", "なんたらかんたらなんたらかんたらなんたらかんたら"));
		galleryList.add(new Gallery(3, "画像3", "3.jpg", "なんたらかんたらなんたらかんたらなんたらかんたら"));
		galleryList.add(new Gallery(4, "画像4", "4.jpg", "なんたらかんたらなんたらかんたらなんたらかんたら"));
		galleryList.add(new Gallery(5, "画像5", "5.jpg", "なんたらかんたらなんたらかんたらなんたらかんたら"));
		galleryList.add(new Gallery(6, "画像6", "6.jpg", "なんたらかんたらなんたらかんたらなんたらかんたら"));
		
		return galleryList;
	}
}
