package model;

import java.io.Serializable;

public class Member implements Serializable {
	private int id;
	private String name;
	private int age;
	private String imgSrc;
	private String comment;

	public Member() {}

	/**
	 * @param id
	 * @param name
	 * @param age
	 * @param imgSrc
	 * @param comment
	 */
	public Member(int id, String name, int age, String imgSrc, String comment) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.imgSrc = imgSrc;
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getImgSrc() {
		return imgSrc;
	}

	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
