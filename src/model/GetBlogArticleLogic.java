package model;

import dao.BlogDAO;

public class GetBlogArticleLogic {
	public Blog execute(int id) {
		BlogDAO dao = new BlogDAO();
		return dao.find(id);
	}
}
