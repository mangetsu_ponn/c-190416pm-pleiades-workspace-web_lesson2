package model;

import java.io.Serializable;

public class Blog implements Serializable {
	private int id;
	private String createAt;	// 日付文字列
	private String title;		// 記事タイトル
	private String article;		// 記事本文

	public Blog() {}

	/**IDは自動生成されるので必要なし。
	 * @param createAt
	 * @param title
	 * @param article
	 */


	public Blog(String createAt, String title, String article) {
		super();
		this.createAt = createAt;
		this.title = title;
		this.article = article;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}
}
