package servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Blog;
import model.PostBlogLogic;

/**
 * Servlet implementation class BlogCreate
 */
@WebServlet("/blog_create")
public class BlogCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// フォームの初期値、本日の日付をリクエストスコープに登録
		Date date = new Date();

		// DB登録するためにyyyy-MM-dd形式にする
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = sdf.format(date);
		request.setAttribute("now", dateString);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/blog_create.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ブログクラスを宣言
		Blog blog = null;

		// フォーム情報を取得
		// 日付を文字列で取得
		String createAt = request.getParameter("create_at");
		String title = request.getParameter("title");
		String article =request.getParameter("article");

		// 記事インスタンスを作成
		blog =new Blog(createAt, title, article);

		// 記事インスタンスをDBに登録
		PostBlogLogic postBlogLogic = new PostBlogLogic();
		postBlogLogic.execute(blog);

		// ブログ一覧にリダイレクト
		response.sendRedirect("blog");
	}
}
