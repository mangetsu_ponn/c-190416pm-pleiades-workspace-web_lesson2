<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp" %>
<c:set var="pageName">ブログ一覧</c:set>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<title>${ pageName }:${ siteName }</title>
</head>
<body>
	<!-- ページヘッダー -->
	<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
	</header>
	<!-- ページヘッダー// -->

	<!-- ページコンテンツ -->
	<article class="container my-4 py-4">
		<section class="row">
		<!-- 以下メインコンテンツ -->
		<h1 class="col-12 border-bottom my-4">${ pageName }</h1>
		<ul class="list-group col-12">
			<c:forEach var="blog" items="${ blogList }">
				<li class="list-group-item list-group-item-action w-50">
				<a href="${ sitePath }/article?id=${ blog.id }" class="text-dark">
				<time>${ blog.createAt }</time>
				<h2 class="h4">${ blog.title }</h2>
				<p class="text-truncate">${ blog.article }</p>
				</a>
				</li>
			</c:forEach>
		</ul>
		</section>
		<div>
			<p class="text-right">
				<a href="${ sitePath }/blog_create">ブログ作成</a>
			</p>
		</div>
	</article>
	<!-- ページコンテンツ// -->
	<!-- ページフッター -->
	<footer class="contain-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
	</footer>
	<!-- ページフッター// -->
</body>
</html>
