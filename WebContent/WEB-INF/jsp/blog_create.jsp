<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp" %>
<c:set var="pageName">ブログ記事作成</c:set>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<title>${ pageName }:${ siteName }</title>
</head>
<body>
	<!-- ページヘッダー -->
	<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
	</header>
	<!-- ページヘッダー// -->

	<!-- ページコンテンツ -->
	<article class="container my-4 py-4">
		<section class="row">
		<!-- 以下メインコンテンツ -->
		<h1 class="col-12 border-bottom my-4">${ pageName }</h1>
		<form action="${ sitePath }/blog_create" method="post" class="col-12">
			<div class="form-group">
				<label for="create_at">作成日付</label><input type="date" name="create_at" id="create_at" class="form-control w-25" value="${ now }" placeholder="yyyy/mm/dd" />
			</div>
			<div class="form-group">
				<label for="title">記事タイトル</label><input type="text" name="title" id="title" class="form-control w-75" />
			</div>
			<div class="form-group">
				<label for="article"></label><textarea name="article" id="article" cols="30" rows="20" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-success btn-lg">記事を作成</button>
			</div>
		</form>
		</section>
		<div>
			<p class="text-right">
				<a href="${ sitePath }/blog">ブログ一覧へ</a>
			</p>
		</div>
	</article>
	<!-- ページコンテンツ// -->
	<!-- ページフッター -->
	<footer class="contain-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
	</footer>
	<!-- ページフッター// -->
</body>
</html>
