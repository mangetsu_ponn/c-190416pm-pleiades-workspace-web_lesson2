<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp" %>
<c:set var="pageName">${ member.name }のページ</c:set>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<title>${ pageName }:${ siteName }</title>
</head>
<body>
	<!-- ページヘッダー -->
	<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
	</header>
	<!-- ページヘッダー// -->

	<!-- ページコンテンツ -->
	<article class="container my-4 py-4">
		<section class="row">
		<!-- 以下メインコンテンツ -->
		<h1 class="col-12 border-bottom my-4">${ member.name }のページ</h1>
		<div class="media col-12">
			<img class="mr-3" src="${ sitePath }/img/${ member.imgSrc }" alt="${ member.name }の画像" style="max-width:320px;">
			<div class="media-body">
				<h5 class="mt-0">${ member.name }</h5>
				<p>年齢:${ member.age }</p>
				<p>${ member.comment }</p>
			</div>
		</div>
		</section>
		<div>
			<p class="text-right">
				<a href="${ sitePath }/member" class="btn btn-link">メンバーページに戻る</a>
			</p>
		</div>
	</article>
	<!-- ページコンテンツ// -->
	<!-- ページフッター -->
	<footer class="contain-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
	</footer>
	<!-- ページフッター// -->
</body>
</html>
