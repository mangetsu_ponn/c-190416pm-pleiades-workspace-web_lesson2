<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp" %>
<c:set var="pageName">記事が見つかりません</c:set>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<c:choose>
	<c:when test="${ blog == null }">
		<!-- 該当記事無し -->
		<title>${ pageName }:${ siteName }</title>
	</c:when>
	<c:otherwise>
		<!-- 該当記事有り -->
		<title>${ blog.title }:${ siteName }</title>
	</c:otherwise>
</c:choose>
</head>
<body>
	<!-- ページヘッダー -->
	<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
	</header>
	<!-- ページヘッダー// -->

	<!-- ページコンテンツ -->
	<article class="container my-4 py-4">
		<section class="row">
		<!-- 以下メインコンテンツ -->
		<c:choose>
			<c:when test="${ blog == null }">
				<!-- 該当記事無し -->
				<h1 class="col-12 border-bottom my-4">${ pageName }</h1>
			</c:when>
			<c:otherwise>
				<!-- 該当記事有り -->
				<div class="col-12">
					<time>${ blog.createAt }</time>
				</div>
				<h1 class="col-12 border-bottom my-4">${ blog.title }</h1>
				<p>${ blog.article }</p>
			</c:otherwise>
		</c:choose>
		</section>
		<div>
			<p class="text-right">
				<a href="${ sitePath }/blog">ブログ一覧へ</a>
			</p>
		</div>
	</article>
	<!-- ページコンテンツ// -->
	<!-- ページフッター -->
	<footer class="contain-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
	</footer>
	<!-- ページフッター// -->
</body>
</html>
