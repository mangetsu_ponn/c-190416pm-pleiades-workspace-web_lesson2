<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp" %>
<c:set var="pageName">スッキリメンバー一覧</c:set>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<title>${ pageName }:${ siteName }</title>
</head>
<body>
	<!-- ページヘッダー -->
	<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
	</header>
	<!-- ページヘッダー// -->

	<!-- ページコンテンツ -->
	<article class="container my-4 py-4">
		<section class="row">
		<!-- 以下メインコンテンツ -->
		<h1 class="col-12 border-bottom my-4">${ pageName }</h1>
		<!-- メンバー一覧 -->
		<c:forEach var="member" items="${ memberList }">
			<div class="card col-3">
				<img src="${ sitePath }/img/${ member.imgSrc }" alt="${ member.name }の画像" class="card-img-top" style="max-width:300px;" />
				<div class="card-body">
					<h3 class="card-title">${ member.name }</h3>
					<p class="card-text">年齢:${ member.age }</p>
				</div>
				<div class="card-footer">
					<a href="${ sitePath }/member?id=${ member.id }" class="btn btn-primary btn-block">about</a>
				</div>
			</div>
		</c:forEach>
		</section>
	</article>
	<!-- ページコンテンツ// -->
	<!-- ページフッター -->
	<footer class="container-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
	</footer>
	<!-- ページフッター// -->
</body>
</html>
