<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- サイト名変数 --%>
<c:set var="siteName">スッキリコーポレーション</c:set>

<%-- サイトURL変数 --%>
<c:set var="sitePath">http://localhost:8080/web_lesson2</c:set>
