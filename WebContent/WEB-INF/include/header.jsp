<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
	<a class="navbar-brand" href="/">${ siteName }</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
	data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
	aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="${ sitePath }/">Home</a></li>
			<li class="nav-item"><a class="nav-link disabled" href="${ sitePath }/about">About</a></li>
			<li class="nav-item"><a class="nav-link" href="${ sitePath }/member">Member</a></li>
			<li class="nav-item"><a class="nav-link" href="${ sitePath }/gallery">Gallery</a></li>
			<li class="nav-item"><a class="nav-link" href="${ sitePath }/blog">blog</a></li>
		</ul>
	</div>
</nav>
