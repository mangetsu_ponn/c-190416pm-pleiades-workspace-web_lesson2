<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<ul class="nav mx-auto w-50">
	<li class="nav-item"><a class="nav-link" href="${ sitePath }/">Home</a></li>
	<li class="nav-item"><a class="nav-link disabled" href="${ sitePath }/about">About</a></li>
	<li class="nav-item"><a class="nav-link" href="${ sitePath }/member">Member</a></li>
	<li class="nav-item"><a class="nav-link" href="${ sitePath }/gallery">Gallery</a></li>
	<li class="nav-item"><a class="nav-link" href="${ sitePath }/blog">blog</a></li>
</ul>
<div class="text-center">
	<small>&copy;${ siteName } all right reserved</small>
</div>
