<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp" %>
<c:set var="pageName">トップページ</c:set>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<title>${ pageName }:${ siteName }</title>
</head>
<body>
	<!-- ページヘッダー -->
	<header class="container-fruid">
	<%@ include file="/WEB-INF/include/header.jsp" %>
	</header>
	<!-- ページヘッダー// -->

	<!-- ページコンテンツ -->
	<!-- ジャンボトロン -->
	<div class="jumbotron jumbotron-fruid">
		<div class="container py-4 my-4">
			<h1 class="display-4">${ siteName }</h1>
			<p class="lead">なんたらかんたらなんたらかんたらなんたらかんたら</p>
		</div>
	</div>
	<!-- ジャンボトロン// -->
	<article class="container my-4">
		<section class="row">
		<!-- 以下メインコンテンツ -->
		<!-- イントロダクション -->
		<h1 class="col-12 border-bottom my-4">${ pageName }</h1>
		<div class="col-4 text-center">
			<img src="img/1.jpg" alt="" class="rounded-circle" width="140" height="140" />
			<h2 class="h2">コンテンツ1</h2>
			<p>なんたらかんたらなんたらかんたら</p>
			<a href="#contents1" class="btn btn-primary">コンテンツ1へ</a>
		</div>
		<div class="col-4 text-center">
			<img src="img/2.jpg" alt="" class="rounded-circle" width="140" height="140" />
			<h2 class="h2">コンテンツ2</h2>
			<p>なんたらかんたらなんたらかんたら</p>
			<a href="#contents2" class="btn btn-primary">コンテンツ2へ</a>
		</div>
		<div class="col-4 text-center">
			<img src="img/3.jpg" alt="" class="rounded-circle" width="140" height="140" />
			<h2 class="h2">コンテンツ3</h2>
			<p>なんたらかんたらなんたらかんたら</p>
			<a href="#contents3" class="btn btn-primary">コンテンツ3へ</a>
		</div>
		</section>
		<!-- イントロダクション// -->
		<!-- コンテンツ1 -->
		<div class="row my-4">
			<section class="col-12">
			<h2 class="h2" id="contents1">コンテンツ1</h2>
			<p>あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ2 -->
		<div class="row my-4">
			<section class="col-12">
			<h2 class="h2" id="contents2">コンテンツ2</h2>
			<p>あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ3 -->
		<div class="row my-4">
			<section class="col-12">
			<h2 class="h2" id="contents3">コンテンツ3</h2>
			<p>あとは好きにしてください。</p>
			</section>
		</div>
	</article>
	<!-- ページコンテンツ// -->
	<!-- ページフッター -->
	<footer class="contain-fruid py-4 bg-light">
	<%@ include file="/WEB-INF/include/footer.jsp" %>
	</footer>
	<!-- ページフッター// -->
</body>
</html>
